package com.Amelie;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class demoDropwizardApplication extends Application<demoDropwizardConfiguration> {

    public static void main(final String[] args) throws Exception {
        new demoDropwizardApplication().run(args);
    }

    @Override
    public String getName() {
        return "hello-world";
    }


    // configure aspects of the application required before the application is run, like bundles, configuration source providers, etc
    @Override
    public void initialize(final Bootstrap<demoDropwizardConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(demoDropwizardConfiguration configuration, Environment environment) throws Exception {
        // TODO auto-generated

    }

}
